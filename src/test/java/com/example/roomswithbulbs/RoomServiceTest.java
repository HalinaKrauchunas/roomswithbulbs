package com.example.roomswithbulbs;

import com.example.roomswithbulbs.entity.Country;
import com.example.roomswithbulbs.entity.Room;
import com.example.roomswithbulbs.repo.RoomRepository;
import com.example.roomswithbulbs.service.RoomService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomServiceTest {

    @Autowired
    private RoomService roomService;

    @MockBean
    private RoomRepository roomRepository;

    @Test
    public void saveRoom() {
        Room room = new Room();
        Country country = new Country();

        roomService.addRoomToDb(room, country);

        Assert.assertEquals(room.getCountry(), country);
        Assert.assertTrue(country.getRooms().contains(room));
        Mockito.verify(roomRepository, Mockito.times(2)).save(room);
    }
}
