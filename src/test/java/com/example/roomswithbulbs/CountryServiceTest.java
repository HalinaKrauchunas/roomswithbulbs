package com.example.roomswithbulbs;

import com.example.roomswithbulbs.entity.Country;
import com.example.roomswithbulbs.repo.CountryRepository;
import com.example.roomswithbulbs.service.CountryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountryServiceTest {

    @Autowired
    private CountryService countryService;

    @MockBean
    private CountryRepository countryRepository;

    @Test
    public void saveCountry() {
        Country country = new Country("Belarus");
        country.setId(1L);

        countryService.save(country);
        Mockito.verify(countryRepository, Mockito.times(1)).save(country);
    }
}