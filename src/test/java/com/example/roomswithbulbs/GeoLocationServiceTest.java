package com.example.roomswithbulbs;

import com.example.roomswithbulbs.service.RawDBDemoGeoIPLocationService;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeoLocationServiceTest {

    @Autowired
    private RawDBDemoGeoIPLocationService locationService;

    @Test
    public void getLocation() throws IOException, GeoIp2Exception {
        String ip = "116.73.210.21";
        Assert.assertEquals(locationService.getLocation(ip), "India");
    }
}


