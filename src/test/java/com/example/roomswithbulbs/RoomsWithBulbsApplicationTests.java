package com.example.roomswithbulbs;

import com.example.roomswithbulbs.controller.HomeController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class RoomsWithBulbsApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HomeController homeController;

    @Test
    void contextLoads() {
        assertThat(homeController).isNotNull();
    }

    @Test
    void homePage() throws Exception {
        this.mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Add new room")))
                .andExpect(content().string(containsString("Add new country")))
                .andExpect(content().string(containsString("Go")));
    }

    @Test
    void homePageList() throws Exception {
        this.mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@id=\"td\"]").nodeCount(5));
    }

    @Test
    void roomPage() throws Exception {
        this.mockMvc.perform(get("/goToRoom?roomId=8"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("home")))
                .andExpect(content().string(containsString("connect")))
                .andExpect(content().string(containsString("disconnect")));
    }
}
