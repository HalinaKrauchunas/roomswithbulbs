CREATE TABLE rooms.countries
(
    id  bigint AUTO_INCREMENT,
    country_name varchar(80),
    PRIMARY KEY (id)
);

CREATE TABLE rooms.rooms
(
    id         bigint AUTO_INCREMENT,
    name       varchar(80),
    lamp_active tinyint(1),
    country_id bigint,
    PRIMARY KEY (id),
    FOREIGN KEY (country_id) REFERENCES rooms.countries (id)
);


