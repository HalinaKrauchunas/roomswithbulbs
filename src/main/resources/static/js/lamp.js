let stompClient = null;

function setConnected(connected) {
    document.getElementById('connect').disabled = connected;
    document.getElementById('disconnect').disabled = !connected;
    document.getElementById('conversationDiv').style.visibility
        = connected ? 'visible' : 'hidden';
}

function connect() {
    let socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function () {
        setConnected(true);
        let roomId = document.getElementById("roomId").value;
        stompClient.subscribe('/topic/activity/' + roomId, function (room) {
                showInfo(JSON.parse(room.body));
            }
        );
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
}

function showInfo(room) {
    let lamp = {lampIsActive: room.lampIsActive};
    let nameRoom = {name: room.name};
    const image = document.getElementById('img')
    document.getElementById("container").appendChild(image)
    let info = document.getElementById("info");
    if (lamp.lampIsActive === true) {
        info.innerText = 'A lamp is burning in the room "' + nameRoom.name + '"';
        image.src = '/images/yes.png';
    } else {
        image.src = '/images/no.png';
        info.innerText = 'A lamp is not lit in the room "' + nameRoom.name + '"';
    }
}

function switchLamp() {
    const roomId = document.getElementById("roomId").value;
    stompClient.send("/app/changeLamp/" + roomId, {});
}