package com.example.roomswithbulbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomsWithBulbsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoomsWithBulbsApplication.class, args);
    }
}
