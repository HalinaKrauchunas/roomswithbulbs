package com.example.roomswithbulbs.repo;

import com.example.roomswithbulbs.entity.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {

    Page<Room> findAll(Pageable pageable);
    Room findByName(String name);

}
