package com.example.roomswithbulbs.repo;

import com.example.roomswithbulbs.entity.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long> {

    Country findByCountryName(String countryName);

}
