package com.example.roomswithbulbs.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@Table(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    @NotBlank(message = "Поле не заполнено")
    private String name;

    @Column(name = "lamp_active")
    private boolean lampIsActive;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "country_id")
    private Country country;

    public Room() {
    }

    public Room(Long id, String name, boolean lampIsActive, Country country) {
        this.id = id;
        this.name = name;
        this.lampIsActive = lampIsActive;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lampIsActive=" + lampIsActive +
                ", country=" + country +
                '}';
    }
}
