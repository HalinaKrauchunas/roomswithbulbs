package com.example.roomswithbulbs.entity;

public class OutputRoom {

    private Long id;
    private String name;
    private boolean lampIsActive;
    private String countryName;

    public OutputRoom(Long id, String name, boolean lampIsActive, String countryName) {
        this.id = id;
        this.name = name;
        this.lampIsActive = lampIsActive;
        this.countryName = countryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLampIsActive() {
        return lampIsActive;
    }

    public void setLampIsActive(boolean lampIsActive) {
        this.lampIsActive = lampIsActive;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String toString() {
        return "OutputRoom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lampIsActive=" + lampIsActive +
                ", countryName='" + countryName + '\'' +
                '}';
    }
}