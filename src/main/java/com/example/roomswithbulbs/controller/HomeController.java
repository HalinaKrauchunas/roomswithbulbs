package com.example.roomswithbulbs.controller;

import com.example.roomswithbulbs.entity.Country;
import com.example.roomswithbulbs.entity.OutputRoom;
import com.example.roomswithbulbs.entity.Room;
import com.example.roomswithbulbs.service.CountryService;
import com.example.roomswithbulbs.service.RawDBDemoGeoIPLocationService;
import com.example.roomswithbulbs.service.RoomService;
import com.example.roomswithbulbs.validation.NameCountryUniquenessValidator;
import com.example.roomswithbulbs.validation.NameRoomUniquenessValidator;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/")
public class HomeController {

    private CountryService countryService;
    private RoomService roomService;
    private final RawDBDemoGeoIPLocationService locationService;
    private NameRoomUniquenessValidator nameRoomUniquenessValidator;
    private NameCountryUniquenessValidator nameCountryUniquenessValidator;

    public HomeController() throws IOException {
        locationService = new RawDBDemoGeoIPLocationService();
    }

    @Autowired
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    @Autowired
    public void setRoomService(RoomService roomService) {
        this.roomService = roomService;
    }

    @Autowired
    public void setNameRoomUniquenessValidator(NameRoomUniquenessValidator nameRoomUniquenessValidator) {
        this.nameRoomUniquenessValidator = nameRoomUniquenessValidator;
    }

    @Autowired
    public void setNameCountryUniquenessValidator(NameCountryUniquenessValidator nameCountryUniquenessValidator) {
        this.nameCountryUniquenessValidator = nameCountryUniquenessValidator;
    }

    @GetMapping()
    public String goToHomePage(

            @RequestParam(value = "size", required = false, defaultValue = "5") Integer size,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            Model model) {
        Page<Room> roomPage;
        roomPage = roomService.findAllRoom(PageRequest.of(page, size));
        model.addAttribute("roomPage", roomPage);
        model.addAttribute("numbers", IntStream.range(0, roomPage.getTotalPages()).toArray());
        return "rooms";
    }

    @GetMapping("/addNewCountry")
    public String addNewCountry(Model model) {

        Country country = new Country();
        model.addAttribute("country", country);
        return "addCountry";
    }

    @GetMapping("/addNewRoom")
    public String addNewRoom(Model model) {

        Room room = new Room();
        model.addAttribute("room", room);
        model.addAttribute("countries", countryService.getAllCountry());
        return "addRoom";
    }

    @PostMapping("/saveCountry")
    public String saveCountry(@Valid @ModelAttribute("country") Country country,
                              BindingResult bindingResult, RedirectAttributes redirectAttrs) {

        nameCountryUniquenessValidator.validate(country, bindingResult);
        if (!bindingResult.hasErrors()) {
            countryService.save(country);
            redirectAttrs.addFlashAttribute("message", "Country " + country.getCountryName() + " added");
            return "redirect:/";
        }
        return "addCountry";
    }

    @PostMapping("/saveRoom")
    public String saveRoom(@Valid @ModelAttribute("room") Room room,
                           BindingResult bindingResult,
                           @RequestParam("idCountry") Long id,
                           Model model) {

        nameRoomUniquenessValidator.validate(room, bindingResult);
        if (!bindingResult.hasErrors()) {
            Country choiceCountry = countryService.getCountryById(id);
            roomService.addRoomToDb(room, choiceCountry);
            countryService.save(choiceCountry);
            return "redirect:/";
        } else {
            model.addAttribute("countries", countryService.getAllCountry());
            return "addRoom";
        }
    }

    @GetMapping("/goToRoom")
    public String goToRoom(@RequestParam("roomId") Long roomId, Model model,
                           HttpServletRequest request, RedirectAttributes redirectAttrs)
            throws IOException, GeoIp2Exception {

//        У меня IP 0:0:0....:1.
//        Если вы используете localhost в своем URI-адресе запроса, то IPv6 - это 0:0:0:0:0:0:0:1.
//        Вместо использования localhost используйте свой IPv4 в URI запроса, чтобы получить правильный IP.
//        Как это сделать?
        String ip = request.getRemoteAddr();
        String location = locationService.getLocation("116.73.210.21");
        Room room = roomService.getRoomById(roomId);
        if (room.getCountry().getCountryName().equals(location)) {
            model.addAttribute("src", roomService.choiceImageForLampAtTheMoment(room));
            model.addAttribute("room", room);
            return "room";
        } else {
            redirectAttrs.addFlashAttribute("message",
                    "" + room.getName() + " is not available for country " + location);
            return "redirect:/#message_section";
        }
    }

    @MessageMapping("/changeLamp/{id}")
    @SendTo("/topic/activity/{id}")
    public OutputRoom clickInRoom(@DestinationVariable String id) {

        Room room = roomService.getRoomById(Long.valueOf(id));
        room.setLampIsActive(!room.isLampIsActive());
        roomService.saveRoom(room);
        return new OutputRoom(
                room.getId(),
                room.getName(),
                room.isLampIsActive(),
                room.getCountry().getCountryName());
    }
}
