package com.example.roomswithbulbs.service;

import com.example.roomswithbulbs.entity.Country;
import com.example.roomswithbulbs.entity.Room;
import com.example.roomswithbulbs.repo.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoomService {

    private RoomRepository roomRepository;

    @Autowired
    public void setRoomRepository(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Transactional
    public Room saveRoom(Room room) {

        roomRepository.save(room);
        return room;
    }

    @Transactional
    public Page<Room> findAllRoom(PageRequest pageRequest) {
        return roomRepository.findAll(pageRequest);
    }

    @Transactional
    public Room getRoomById(Long id) {

        return roomRepository.findById(id).get();
    }

    public String choiceImageForLampAtTheMoment(Room room) {

        if (room.isLampIsActive()) {
            return "/images/yes.png";
        } else {
            return "/images/no.png";
        }
    }

    @Transactional
    public Room findByName(String name) {

        return roomRepository.findByName(name);
    }

    public void addRoomToDb(Room room, Country choiceCountry) {

        saveRoom(room);
        room.setCountry(choiceCountry);
        saveRoom(room);
        choiceCountry.getRooms().add(room);
    }
}
