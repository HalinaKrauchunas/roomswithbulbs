package com.example.roomswithbulbs.service;

import com.example.roomswithbulbs.entity.Country;
import com.example.roomswithbulbs.repo.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CountryService {

    private CountryRepository countryRepository;

    @Autowired
    public void setCountryRepository(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Transactional
    public List<Country> getAllCountry() {
        return StreamSupport.stream(countryRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @Transactional
    public Country getCountryById(Long id) {
        return countryRepository.findById(id).get();
    }

    @Transactional
    public void save(Country choiceCountry) {
        countryRepository.save(choiceCountry);
    }

    @Transactional
    public Country findByName(String name) {
        return countryRepository.findByCountryName(name);
    }
}
