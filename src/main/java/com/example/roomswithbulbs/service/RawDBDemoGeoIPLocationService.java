package com.example.roomswithbulbs.service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

@Service
public class RawDBDemoGeoIPLocationService {

    private final DatabaseReader dbReader;

    public RawDBDemoGeoIPLocationService() throws IOException {
        String separator = File.separator;
        File database = new File(
                "src" + separator + "main" + separator + "resources" +
                        separator + "databaseLocation" + separator + "GeoLite2-Country.mmdb");
        dbReader = new DatabaseReader.Builder(database).build();
    }

    public String getLocation(String ip) throws IOException, GeoIp2Exception {
        InetAddress ipAddress = InetAddress.getByName(ip);
        CountryResponse response = dbReader.country(ipAddress);

        return response.getCountry().getName();
    }
}
