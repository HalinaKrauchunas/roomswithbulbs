package com.example.roomswithbulbs.validation;

import com.example.roomswithbulbs.entity.Room;
import com.example.roomswithbulbs.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class NameRoomUniquenessValidator implements Validator {

    private RoomService roomService;

    @Autowired
    public void setRoomService(RoomService roomService) {
        this.roomService = roomService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Room.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Room room = (Room) target;
        if (roomService.findByName(room.getName()) != null) {
            errors.rejectValue("name", "", "A room with that name exists");
        }
    }
}
