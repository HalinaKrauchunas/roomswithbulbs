package com.example.roomswithbulbs.validation;

import com.example.roomswithbulbs.entity.Country;
import com.example.roomswithbulbs.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class NameCountryUniquenessValidator implements Validator {

    private CountryService countryService;

    @Autowired
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Country.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Country country = (Country) target;
        if (countryService.findByName(country.getCountryName()) != null) {
            errors.rejectValue("countryName", "", "A room with that name exists");
        }
    }
}
